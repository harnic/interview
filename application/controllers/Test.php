<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends CI_Controller {
	public function firstInterview()
	{
        $this->load->view('interview1');
	}
	public function secondInterview()
	{
        $data['items'] = $this->M_test->get_items();
		$this->load->view('interview2',$data);
	}
	public function thirdInterview()
	{
		$this->load->view('interview3');
	}
}
