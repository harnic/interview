<?php

class M_test extends CI_Model
{
    function get_items(){
        $this->db->select('*');
        $this->db->from('items');
        $result = $this->db->get();
        return $result->result();
    }
}