/*
 Navicat Premium Data Transfer

 Source Server         : XAMPP
 Source Server Type    : MySQL
 Source Server Version : 80016
 Source Host           : 127.0.0.1:3306
 Source Schema         : interview

 Target Server Type    : MySQL
 Target Server Version : 80016
 File Encoding         : 65001

 Date: 29/06/2020 00:19:00
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for items
-- ----------------------------
DROP TABLE IF EXISTS `items`;
CREATE TABLE `items`  (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`item_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of items
-- ----------------------------
INSERT INTO `items` VALUES (1, 'Item A');
INSERT INTO `items` VALUES (2, 'Item B');
INSERT INTO `items` VALUES (3, 'Item C');
INSERT INTO `items` VALUES (4, 'Item D');
INSERT INTO `items` VALUES (5, 'Item E');
INSERT INTO `items` VALUES (6, 'Item F');
INSERT INTO `items` VALUES (7, 'Item G');
INSERT INTO `items` VALUES (8, 'Item H');
INSERT INTO `items` VALUES (9, 'Item I');
INSERT INTO `items` VALUES (10, 'Item J');

SET FOREIGN_KEY_CHECKS = 1;
