###################
Selamat Datang Kandidat
###################

Repository ini adalah program dummy yang dibuat untuk kepentingan test interview web developer HARNIC.ID

*******************
Installation
*******************
Installasinya cukup simple.
    1. Clone project ini ke komputer local, bisa disimpan di folder htdocs xampp atau lainnya, tergantung settingan environment anda.
    2. Buat database MySQL dengan nama "interview"
    3. Jalankan script SQL "interview.sql" di mysql client anda (untuk membuat table-table yang diperlukan aplikasi di dalam database interview)
    4. Sesuaikan config database di file application/config/database.php
    5. jalankan web di browser dan ikuti instruksi yang tampil


Selamat Bermain!